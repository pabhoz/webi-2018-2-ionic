import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';

/**
 * Generated class for the NavigationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-navigation',
  templateUrl: 'navigation.html',
})
export class NavigationPage {
  navigationPage = NavigationPage;
  n: number = 0;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.n = (navParams.get('data')) ? navParams.get('data') : 0;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NavigationPage');
  }

  grinGoHome(){
    this.navCtrl.setRoot(HomePage);
  }

}
