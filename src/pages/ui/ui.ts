import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the UiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ui',
  templateUrl: 'ui.html',
})
export class UiPage {

  player: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.player = new Audio();
    this.player.src = "assets/songs/1.mp3";
    console.log(this.player);
  }

  playBlue(){
    if(this.player.paused){
      this.player.play();
    }else{
      this.player.pause();
    }
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UiPage');
  }

}
